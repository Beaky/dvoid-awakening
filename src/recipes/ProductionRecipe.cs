namespace View.PlanetView.Buildings.ProductionRecipes
{
    using Godot;
    using StrategicResources;
    public partial class ProductionRecipe : Resource
    {
        [Export]
        public StrategicResource[] InputResources { get; private set; }
        [Export]
        public ProducedResource[] OutputResources { get; private set; }
    }
}
