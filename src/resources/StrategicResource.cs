namespace StrategicResources
{
    using Godot;
    using Godot.Collections;
    using System.Collections.Generic;
    using View.PlanetView.Structures;

    public partial class StrategicResource : Resource
    {
        [Signal]
        public delegate void InterimDestinationLostEventHandler(StrategicResource resource);
        [Signal]
        public delegate void DestinationLostEventHandler(StrategicResource resource);

        [Export]
        public int Amount { get; set; } = 0;

        [Export]
        public string HumanReadableName { get; set; } = "";

        public bool IsOrphaned { get; set; } = false;
        public List<PlanetStructure> Path3D;
        public double TransportTimer { get; set; }

        private void OnResourceInterimDestinationLostEventHandler(PlanetStructure interimDestination)
        {
            Path3D.Remove(interimDestination);
            if (Path3D.Count == 0 || interimDestination == Path3D[Path3D.Count - 1])
            {
                EmitSignal("DestinationLost", this);
            }
            else if (interimDestination != Path3D[0])
            {
                EmitSignal("InterimDestinationLost", this);
            }
        }

        public void ClearPathSignals()
        {
            Array<Dictionary> pathSignals = GetIncomingConnections();
            foreach (Dictionary signal in pathSignals)
            {
                ((Node3D) signal["source"]).Disconnect((string) signal["signal_name"], new Callable(this, (string) signal["method_name"]));
            }
        }
    }
}
