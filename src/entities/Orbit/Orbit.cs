using System;
using Godot;

namespace Entities
{
    [Tool]
    public partial class Orbit : Resource
    {
        [Export]
        public Vector3 Semimajor
        {
            get { return _semimajor; }
            set
            {
                _semimajor = value;
                EmitSignal(SignalName.Changed);
            }
        }
        [Export]
        public Vector3 Semiminor
        {
            get { return _semiminor; }
            set
            {
                _semiminor = value;
                EmitSignal(SignalName.Changed);
            }
        }
        [Export]
        public Vector3 Origin
        {
            get { return _origin; }
            set
            {
                _origin = value;
                EmitSignal(SignalName.Changed);
            }
        }
        [Export(PropertyHint.Range, "0,360,radians_as_degrees")]
        public double Argument
        {
            get { return _argument; }
            set
            {
                _argument = value;
                EmitSignal(SignalName.Changed);
            }
        }

        private Vector3 _semimajor = new(0, 0, 0);
        private Vector3 _semiminor = new(0, 0, 0);
        private Vector3 _origin = new (0, 0, 0);
        private double _argument = 0.0;

		public Vector3 CalculateOrbitPosition(double t)
		{
			return Origin + ((float) Math.Cos(t) * Semimajor) + ((float) Math.Sin(t) * Semiminor);
		}
    }
}
