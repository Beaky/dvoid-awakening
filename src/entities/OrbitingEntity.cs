using System;
using System.Collections.Generic;
using Godot;

namespace Entities
{
    public partial class OrbitingEntity : CharacterBody3D
    {

        public int Id { get; set; } = 1;
		private Orbit _orbit;
		[Export]
		public Orbit Orbit
		{
			get { return _orbit; }
			set
			{
				_orbit = value;
				if (value != null)
				{
					value.Changed += () => InitOrbit();
					InitOrbit();
				}
			}
		}
		protected Node3D _orbitLine;

        public override void _Ready()
        {
            base._Ready();
            InitOrbit();
        }

		private void InitOrbit()
		{
			if (Orbit == null)
			{
				GD.PrintErr("No orbit defined for planet " + Id);
				return;
			}
			Position = Orbit.CalculateOrbitPosition(Orbit.Argument);
			int orbitResolution = 100;
			List<Vector3> points = new();
			for (int i = 0; i <= orbitResolution; i++)
			{
				double t = i * (2 * Math.PI / orbitResolution);
				points.Add(Orbit.CalculateOrbitPosition(t));
			}
			_orbitLine ??= GetNodeOrNull<Node3D>("OrbitLine");
			if (_orbitLine == null)
			{
				GDScript line3DScript = GD.Load<GDScript>("res://addons/godot-polyliner/Line3D/Line3D.gd");
				_orbitLine = (Node3D) line3DScript.New();
				AddChild(_orbitLine);
			}
			Shader line3DShader = GD.Load<Shader>("res://addons/godot-polyliner/shaders/line_glow.tres");
			ShaderMaterial shaderMaterial = new ();
			shaderMaterial.Shader = line3DShader;
			_orbitLine.Position = Orbit.Origin - Position - Orbit.Origin;
			_orbitLine.Call("set_material", shaderMaterial);
			_orbitLine.Call("set_points", points.ToArray());
		}
    }
}