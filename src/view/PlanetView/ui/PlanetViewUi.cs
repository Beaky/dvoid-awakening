namespace View.PlanetView.Ui
{
	using Godot;
	using View.ViewStates;
	using View.PlanetView.Structures;

	public partial class PlanetViewUi : Control
	{
		[Export]
		private Vector2 _buttonSize = new Vector2(200, 36);

		private HBoxContainer categoriesContainer;

		public override void _Ready()
		{
			categoriesContainer = GetNode<HBoxContainer>("Actions/Construction/Panel/HBoxContainer");
		}

		/// <summary>
		/// Populates the Construction section of the UI with buttons for each city.
		/// </summary>
		/// <param name="cityModels"></param>
		public void BuildConstructionButtons(Node cityModels)
		{
			foreach(Node category in cityModels.GetChildren())
			{
				if(category.Name == "Hidden")
				{
					continue;
				}
				VBoxContainer buildingContainer = new VBoxContainer();
				buildingContainer.Alignment = BoxContainer.AlignmentMode.End;
				buildingContainer.CustomMinimumSize = _buttonSize;

				for (int i = 0; i < category.GetChildCount(); i++)
				{
					PlanetBuilding building = category.GetChild<PlanetBuilding>(category.GetChildCount() - 1 - i);
					Button buildingButton = new Button();
					buildingButton.Text = building.HumanReadableName;
					buildingButton.CustomMinimumSize = _buttonSize;
					buildingButton.Pressed += () => GetParent<PlanetViewState>().OnConstructionButtonPressed(building);
					buildingContainer.AddChild(buildingButton);
				}
				categoriesContainer.AddChild(buildingContainer);

				// Category button, for hovering to expand the selection.
				Button categoryButton = new Button();
				categoryButton.CustomMinimumSize = _buttonSize;
				categoryButton.Text = category.Name;
				buildingContainer.AddChild(categoryButton);
			}
		}

		public void ShowPlanetStructureUi(PlanetStructure structure)
		{
			GetNode<PlanetStructureUiContainer>("PlanetStructureUiContainer").ShowInfo(structure);
		}

		public bool PlanetBuildingUiInfoIsVisible()
		{
			return GetNode<PlanetStructureUiContainer>("PlanetStructureUiContainer").Visible;
		}
	}
}
