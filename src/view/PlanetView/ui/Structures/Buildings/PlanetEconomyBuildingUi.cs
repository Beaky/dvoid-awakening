namespace View.PlanetView.Buildings.Ui
{
	using System;
	using Godot;
	using StrategicResources;
	using View.PlanetView.Structures;
	using System.Collections.Generic;

	public partial class PlanetEconomyBuildingUi : PlanetStructureUi
	{
		[Export]
		private NodePath _inputContainerPath = "";

		[Export]
		private NodePath _outputContainerPath = "";

		[Export]
		private NodePath _inputLabelPath = "";

		[Export]
		private NodePath _outputLabelPath = "";

		[Export]
		private NodePath _inputResourceContainerPath = "";

		[Export]
		private NodePath _outputResourceContainerPath = "";

		[Export]
		private NodePath _resourceTemplatePath = "";

		protected VBoxContainer _inputContainer;
		protected VBoxContainer _outputContainer;
		protected Label _inputLabel;
		protected Label _outputLabel;
		protected VBoxContainer _inputResourceContainer;
		protected VBoxContainer _outputResourceContainer;
		protected HBoxContainer _resourceTemplate;
		protected enum LabelType {Input, Output}
		protected Dictionary<string, HBoxContainer>[] _resourceLabels = {new(), new()};

		public override void _Ready()
		{
			_inputContainer = GetNodeOrNull<VBoxContainer>(_inputContainerPath);
			_outputContainer = GetNodeOrNull<VBoxContainer>(_outputContainerPath);
			_inputLabel = GetNodeOrNull<Label>(_inputLabelPath);
			_outputLabel = GetNodeOrNull<Label>(_outputLabelPath);
			_inputResourceContainer = GetNodeOrNull<VBoxContainer>(_inputResourceContainerPath);
			_outputResourceContainer = GetNodeOrNull<VBoxContainer>(_outputResourceContainerPath);
			_resourceTemplate = GetNodeOrNull<HBoxContainer>(_resourceTemplatePath);
			base._Ready();
		}

		protected override void BuildUi(bool firstBuild)
		{
			PlanetEconomyBuilding economyBuilding = (PlanetEconomyBuilding) _structure;
			PlanetEconomyBuildingUiConfig uiConfig = (PlanetEconomyBuildingUiConfig) economyBuilding.UiConfig;
			if(firstBuild)
			{
				ClearLabels();
				_inputLabel.Text = ((PlanetEconomyBuildingUiConfig) _structure.UiConfig).InputLabelText;
				_outputLabel.Text = ((PlanetEconomyBuildingUiConfig) _structure.UiConfig).OutputLabelText;
				if (economyBuilding.InputStorageSize == 0 && economyBuilding.OutputStorageSize == 0)
				{
					GD.Print("[INFO] Building neither has Input nor Output storage capacity, UI will be empty.");
				}
			}
			if(uiConfig.ShowInput)
			{
				_inputContainer.Visible = true;
				foreach(StrategicResource resource in economyBuilding.StoredInputResources.Values)
				{
					HBoxContainer container = null;
					// Check if we already have a Container for this resource
					if(_resourceLabels[(int) LabelType.Input].ContainsKey(resource.ResourceName))
					{
						container = _resourceLabels[(int) LabelType.Input][resource.ResourceName];
					}
					if(uiConfig.HideEmptyResources && resource.Amount == 0)
					{
						if(container != null)
						{
							container.Visible = false;
						}
						continue;
					}
					else if(container != null)
					{
						container.GetChild<Label>(1).Text = resource.Amount.ToString();
						container.Visible = true;
					}
					else
					{
						HBoxContainer resourceLabel = prepareResourceLabel(resource);
						_inputResourceContainer.AddChild(resourceLabel);
						_resourceLabels[(int) LabelType.Input][resource.ResourceName] = resourceLabel;
					}
				}
			}
			else
			{
				_inputContainer.Visible = false;
			}

			if(uiConfig.ShowOutput)
			{
				_outputContainer.Visible = true;
				foreach(StrategicResource resource in economyBuilding.StoredOutputResources.Values)
				{
					HBoxContainer container = null;
					if(_resourceLabels[(int) LabelType.Output].ContainsKey(resource.ResourceName))
					{
						Console.WriteLine(_resourceLabels[(int) LabelType.Output][resource.ResourceName]);
						container = _resourceLabels[(int) LabelType.Output][resource.ResourceName];
					}
					if(uiConfig.HideEmptyResources && resource.Amount == 0)
					{
						if(container != null)
						{
							container.Visible = false;
						}
						continue;
					}
					else if(container != null)
					{
						container.GetChild<Label>(1).Text = resource.Amount.ToString();
						container.Visible = true;
					}
					else
					{
						HBoxContainer resourceLabel = prepareResourceLabel(resource);
						_outputResourceContainer.AddChild(resourceLabel);
						_resourceLabels[(int) LabelType.Output][resource.ResourceName] = resourceLabel;
					}
				}
			}
			else
			{
				_outputContainer.Visible = false;
			}
		}

		protected HBoxContainer prepareResourceLabel(StrategicResource resource)
		{
			HBoxContainer resourceLabel = (HBoxContainer) _resourceTemplate.Duplicate();
			resourceLabel.GetChild<Label>(0).Text = resource.HumanReadableName;
			resourceLabel.GetChild<Label>(1).Text = resource.Amount.ToString();
			resourceLabel.Visible = true;

			return resourceLabel;
		}
		protected virtual void ClearLabels()
		{
			List<HBoxContainer> labels = new List<HBoxContainer>();
			labels.AddRange(_resourceLabels[(int) LabelType.Input].Values);
			labels.AddRange(_resourceLabels[(int) LabelType.Output].Values);
			foreach(HBoxContainer node in labels)
			{
				node.QueueFree();
			}
			_resourceLabels[(int) LabelType.Input].Clear();
			_resourceLabels[(int) LabelType.Output].Clear();
		}
	}
}

