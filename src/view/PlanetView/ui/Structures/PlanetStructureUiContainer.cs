namespace View.PlanetView.Ui
{
	using Godot;
	using System;
	using System.Collections.Generic;
	using StrategicResources;
	using View.PlanetView.Structures;
	using View.PlanetView.Buildings.Ui;

	public partial class PlanetStructureUiContainer : Panel
	{
		private PlanetStructureUi _activeUi;
		private Button _destroyButton;

		public override void _Ready()
		{
			_destroyButton = GetNode<Button>("DestroyButtonContainer/DestroyButton");
			_destroyButton.Connect("pressed", new Callable(this, "OnDestroyButtonPressed"));
			HideInfo();
		}

		public override void _Input(InputEvent @event)
		{
            if (Visible && @event is InputEventMouseButton)
			{
				if (!GetGlobalRect().HasPoint(GetGlobalMousePosition()))
				{
					HideInfo();
				}
			}
		}

		public void ShowInfo(PlanetStructure structure)
		{
			if(structure is PlanetConstructionBuilding cnstrctnBuilding)
			{
				GetNode<Label>("Body/StructureName/StructureName").Text = cnstrctnBuilding.ConstructedBuilding.HumanReadableName + " (under construction)";
			}
			else
			{
				GetNode<Label>("Body/StructureName/StructureName").Text = structure.HumanReadableName;
			}

			switch(structure)
			{
				case PlanetConstructionBuilding constructionBuilding:
					_activeUi = GetNode<PlanetConstructionBuildingUi>("Body/PlanetConstructionBuildingUi");
					break;
				case PlanetTransportHub hub:
					_activeUi = GetNode<PlanetTransportHubUi>("Body/PlanetTransportHubUi");
					break;
				case PlanetEconomyBuilding economyBuilding:
					_activeUi = GetNode<PlanetEconomyBuildingUi>("Body/PlanetEconomyBuildingUi");
					break;
				case PlanetTransportRoute transportRoute:
					_activeUi = GetNode<PlanetTransportRouteUi>("Body/PlanetTransportRouteUi");
					break;
			}
			if(_activeUi == null)
			{
				return;
			}
			_activeUi.ShowInfo(structure);
			Vector2 newRectPosition = GetGlobalMousePosition();
			newRectPosition.Y -= 40; // Compensate for Top Bar
			Position = newRectPosition;
			Visible = true;
		}

		public void HideInfo()
		{
			if(_activeUi != null)
			{
				_activeUi.HideInfo();
				_activeUi = null;
			}
			Visible = false;
		}

		public void OnDestroyButtonPressed()
		{
			_activeUi.OnDestroyButtonPressed();
			HideInfo();
		}
	}
}
