namespace View.PlanetView.Buildings.Ui
{
    using Godot;
    using View.PlanetView.Structures;
    public abstract partial class PlanetStructureUi : VBoxContainer
    {
        protected PlanetStructure _structure;
        protected abstract void BuildUi(bool firstBuild);

        public override void _Ready()
        {
            SetProcess(false);
            Visible = false;
        }

        public override void _Process(double delta)
        {
            BuildUi(false);
        }

        public virtual void ShowInfo(PlanetStructure structure)
        {
            _structure = structure;
            BuildUi(true);
            SetProcess(true);
            Visible = true;
        }

        public virtual void HideInfo()
        {
            _structure = null;
            Visible = false;
            SetProcess(false);
        }

        public virtual void OnDestroyButtonPressed()
        {
            GD.Print(_structure);
            _structure?.Destroy();
        }
    }
}
