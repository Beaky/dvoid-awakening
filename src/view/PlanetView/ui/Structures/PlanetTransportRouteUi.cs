namespace View.PlanetView.Buildings.Ui
{
	using Godot;
	using System.Collections.Generic;
	using StrategicResources;

	public partial class PlanetTransportRouteUi : PlanetStructureUi
	{
		Dictionary<StrategicResource, ProgressBar> _resourceEntries = new Dictionary<StrategicResource, ProgressBar>();
		VBoxContainer _transportedBodyContainer;
		HBoxContainer _buttonContainer;

		public override void _Ready()
		{
			_transportedBodyContainer = GetNode<VBoxContainer>("TransportScrollContainer/TransportBodyContainer");
			((Control) _transportedBodyContainer.GetChild(0)).Visible = false; // Hide the progressbar template
			HideInfo();
		}

		protected override void BuildUi(bool firstBuild)
		{
			if (firstBuild)
			{
				ClearResourceEntries();
			}

			PlanetTransportRoute route = (PlanetTransportRoute) _structure;
			// Check if there are new resources on this route
			foreach (StrategicResource resource in route.TransportedResources)
			{
				if (!_resourceEntries.ContainsKey(resource))
				{
					CreateResourceEntry(route, resource);
				}
			}
			// Find resources that are no longer on this route
			List<StrategicResource> forwardedEntries = new List<StrategicResource>();
			foreach (StrategicResource resource in _resourceEntries.Keys)
			{
				if (resource.Path3D[0] != route)
				{
					_resourceEntries[resource].QueueFree();
					forwardedEntries.Add(resource);
					continue;
				}
				_resourceEntries[resource].Value = resource.TransportTimer / (route.Length / route.TransportSpeed);
			}
			// Remove all resources that are no longer on this route
			foreach (StrategicResource resource in forwardedEntries)
			{
				_resourceEntries.Remove(resource);
			}
		}

		private void CreateResourceEntry(PlanetTransportRoute route, StrategicResource resource)
		{
			ProgressBar progressBar = (ProgressBar) _transportedBodyContainer.GetChild(0).Duplicate();
			progressBar.Value = resource.TransportTimer / (route.Length / route.TransportSpeed);
			progressBar.GetNode<Label>("HSplitContainer/Name").Text = resource.HumanReadableName;
			progressBar.GetNode<Label>("HSplitContainer/Amount").Text = resource.Amount.ToString();
			_transportedBodyContainer.AddChild(progressBar);

			_resourceEntries[resource] = progressBar;
			progressBar.Visible = true;
		}

		private void ClearResourceEntries()
		{
			_resourceEntries = new Dictionary<StrategicResource, ProgressBar>();
			for (int i = 1; i < _transportedBodyContainer.GetChildCount() - 1; i++)
			{
				_transportedBodyContainer.GetChild(i).Free();
			}
		}
	}
}
