namespace View.PlanetView.Structures
{
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System;
	using Godot;
	using StrategicResources;
	using View.PlanetView.Buildings.ProductionRecipes;

	public partial class PlanetProductionBuilding : PlanetEconomyBuilding
	{
		[Export]
		private ProductionRecipe _productionRecipe = null;

		public override void _Ready()
		{
			base._Ready();
			foreach (StrategicResource resource in _productionRecipe.InputResources)
			{
				StrategicResource inputResource = (StrategicResource) resource.Duplicate();
				inputResource.Amount = 0;
				_storedInputResources[inputResource.ResourceName] = inputResource;
			}
			foreach (StrategicResource resource in _productionRecipe.OutputResources)
			{
				StrategicResource outputResource = (StrategicResource) resource.Duplicate();
				outputResource.Amount = 0;
				_storedOutputResources.Add(outputResource.ResourceName, outputResource);
			}
		}

		public override void OnTick()
		{
			ProduceRecipe();
			foreach(StrategicResource resource in StoredOutputResources.Values)
			{
				if (resource.Amount != 0)
				{
					EmitSignal("ProducedResource", this, resource);
				}
			}
		}

		private bool ProduceRecipe()
		{
			// Check if there is enough space to store the output resources.
			GD.Print(_productionRecipe.OutputResources);
			foreach(StrategicResource recipeOutputResource in _productionRecipe.OutputResources)
			{
				// Amount which would be stored in the output resource if production would continue
				int amountAfterProduction = recipeOutputResource.Amount + StoredOutputResources[recipeOutputResource.ResourceName].Amount;
				if(amountAfterProduction > OutputStorageSize)
				{
					return false;
				}
			}

			// Check if we have enough of all input resources.
			foreach (StrategicResource recipeResource in _productionRecipe.InputResources)
			{
				if (StoredInputResources[recipeResource.ResourceName].Amount < recipeResource.Amount)
				{
					return false;
				}
			}
			// If the above loop hasn't returned, we're good to go. Substract from stored resources
			foreach (StrategicResource recipeResource in _productionRecipe.InputResources)
			{
				_storedInputResources[recipeResource.ResourceName].Amount -= recipeResource.Amount;
			}
			foreach(StrategicResource resource in _storedOutputResources.Values)
			{
				// Need to find the resource in the recipe's output array since it's not a dictionary (maybe change that?)
				int outputAmount = 0;
				foreach(StrategicResource recipeOutputResource in _productionRecipe.OutputResources)
				{
					if (recipeOutputResource.ResourceName == resource.ResourceName)
					{
						outputAmount = recipeOutputResource.Amount;
						break;
					}
				}
				resource.Amount = Mathf.Min(resource.Amount + outputAmount, OutputStorageSize);
			}

			return true;
		}
	}
}
