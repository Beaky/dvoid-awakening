namespace View.PlanetView.Structures
{
    using Godot;
    using StrategicResources;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    abstract public partial class PlanetEconomyBuilding : PlanetBuilding
    {
        [Signal]
        public delegate void ProducedResourceEventHandler(PlanetBuilding building, StrategicResource resource);
        [Export]
        public int OutputStorageSize { get; protected set; }= 6;
        [Export]
        public int InputStorageSize { get; protected set; } = 6;

        protected Dictionary<string, StrategicResource> _storedInputResources = new Dictionary<string, StrategicResource>();
        public ReadOnlyDictionary<string, StrategicResource> StoredInputResources { get => new ReadOnlyDictionary<string, StrategicResource>(_storedInputResources); }
        /// <summary>
        /// Amount of resources that are on the way to this building but haven't arrived yet.
        /// </summary>
        /// <value></value>
        public Dictionary<string, int> InboundResourcesAmount { get; set; } = new Dictionary<string, int>();
        protected Dictionary<string, StrategicResource> _storedOutputResources = new Dictionary<string, StrategicResource>();
        public ReadOnlyDictionary<string, StrategicResource> StoredOutputResources { get => new ReadOnlyDictionary<string, StrategicResource>(_storedOutputResources); }

        public virtual void ReceiveResource(StrategicResource resource)
        {
            if (GetFreeResourceCapacity(resource.ResourceName) + InboundResourcesAmount[resource.ResourceName] < resource.Amount)
            {
                GD.Print($"[INFO] \"{GetPath()}\" Received Resource although storage is full!");
                GD.Print($"|- Free storage was {GetFreeResourceCapacity(resource.ResourceName)}, pushed amount was {resource.Amount}");
            }
            if (_storedInputResources.ContainsKey(resource.ResourceName))
            {
                _storedInputResources[resource.ResourceName].Amount += resource.Amount;
            }
            else
            {
                _storedInputResources[resource.ResourceName] = resource;
            }
            InboundResourcesAmount[resource.ResourceName] = Mathf.Max(InboundResourcesAmount[resource.ResourceName] - resource.Amount, 0);
        }


        public virtual int GetFreeResourceCapacity(string resourceName)
        {
            if (InputStorageSize == 0)
            {
                return 0;
            }
            int storedAmount = 0;
            int inboundAmount = 0;
            if (StoredInputResources.ContainsKey(resourceName))
            {
                storedAmount = StoredInputResources[resourceName].Amount;
            }
            if (InboundResourcesAmount.ContainsKey(resourceName))
            {
                inboundAmount = InboundResourcesAmount[resourceName];
            }
            return Mathf.Max(InputStorageSize - (storedAmount + inboundAmount), 0);
        }
    }
}

