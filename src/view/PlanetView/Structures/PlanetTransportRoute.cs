namespace View.PlanetView
{
	using Godot;
	using StrategicResources;
	using View.PlanetView.Structures;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
    using System;

    public partial class PlanetTransportRoute : PlanetStructure
	{
		public PlanetBuilding Start { get; set; }
		public PlanetBuilding Ending { get; set; }

		[Export(PropertyHint.None, "m/s on a TransportRoute, time for transport on a TransportHub")]
		public float TransportSpeed { get; private set; } = 1.0f;

		[Export]
		protected int _resourceCapacity = 3;

		public float TransportTime { get; protected set; }
		protected List<StrategicResource> _transportedResources = new();
		public ReadOnlyCollection<StrategicResource> TransportedResources { get => _transportedResources.AsReadOnly(); }
		public int FreeCapacity { get => _resourceCapacity - _transportedResources.Count; }
		public float Length { get; set; } = 0.0f;

		private float _planetRadius = 1.0f;
		private List<Vector3> _points;
		private MeshInstance3D dbgx;
		private MeshInstance3D dbgy;
		private MeshInstance3D dbgz;

		public override void _Ready()
		{
			CollisionLayer = 0b100;
			base._Ready();
		}
		public override void _Process(double delta)
		{
			TransportResources(delta);
		}

		private void TransportResources(double delta)
		{
			for (int i = _transportedResources.Count - 1; i >= 0; i--) // Iterate backwards so we can remove stuff from the list
			{
				StrategicResource resource = _transportedResources[i];
				resource.TransportTimer += TransportSpeed * delta;
				if (resource.TransportTimer >= TransportTime)
				{
					// If the resource has no destination, try to transport it away.
					if (resource.IsOrphaned)
					{
						resource.EmitSignal("DestinationLost", resource); // TODO: Make this a function in the resource
						if (resource.Amount == 0)
						{
							_transportedResources.RemoveAt(i);
						}
						else
						{
							resource.TransportTimer = 0;
						}
					}
					else
					{
						resource.Path3D.RemoveAt(0);
						// Economy Buildings have storage space reserved for them
						if (resource.Path3D[0] is PlanetEconomyBuilding economyBuilding)
						{
							economyBuilding.ReceiveResource(resource);
						}
						_transportedResources.RemoveAt(i);
					}
				}
			}
		}

		/// <summary>
		/// Accept and store a resource on this route.
		/// </summary>
		/// <param name="resource"></param>
		/// <returns></returns>
		public void ReceiveResource(StrategicResource resource)
		{
			resource.TransportTimer = 0;
			_transportedResources.Add(resource);
		}

		public bool ConnectToBuilding(PlanetBuilding building)
		{
			if (building == null || building == Start)
			{
				return false;
			}
			else if (building.MaximumConnectedRoutes != -1 && building.ConnectedRoutes.Count >= building.MaximumConnectedRoutes)
			{
				return false;
			}
			if (Start == null)
			{
				Start = building;
			}
			else
			{
				Ending = building;
			}
			return true;
		}

		public override void Build()
		{
			dbgx = GetNode<MeshInstance3D>("DBGX");
			dbgy = GetNode<MeshInstance3D>("DBGY");
			dbgz = GetNode<MeshInstance3D>("DBGZ");

			Start.AddRouteConnection(this);
			Ending.AddRouteConnection(this);
			_points = GetPathPoints(Start.GlobalTransform.Origin, Ending.GlobalTransform.Origin);
			// Calculate the length of the path
			Vector3 lastPoint = new();
			foreach (Vector3 point in _points)
			{
				if (lastPoint == Vector3.Zero)
				{
					lastPoint = point;
					continue;
				}
				Length += lastPoint.DistanceTo(point);
				lastPoint = point;
			}
			TransportTime = Length / TransportSpeed;
			BuildRouteCollisions(_points);
			BuildRouteGeometry(_points);
			SetProcess(true);
		}

		private List<Vector3> GetPathPoints(Vector3 from, Vector3 to)
		{
			Vector3 x = from.Normalized();
			Vector3 y = to.Cross(from).Normalized();


			Vector3 z = from.Cross(y).Normalized();
			Transform3D transform = new(new Basis(x, y, z).Orthonormalized(), GlobalTransform.Origin);
			GlobalTransform = transform; // No need to convert the route's points to local space later on.
			Vector3 localFrom = ToLocal(from);
			Vector3 localTo = ToLocal(to);

			Transform3D dbgtransform = dbgx.Transform;
			dbgtransform.Origin = localFrom;
			dbgx.Transform = dbgtransform;
			dbgtransform.Origin = localTo;
			dbgy.Transform = dbgtransform;

			float phiFrom = Mathf.Atan2(localFrom.Z, localFrom.X);
			float phiTo = Mathf.Atan2(localTo.Z, localTo.X);
			float d = Mathf.Abs(phiTo - phiFrom);

			int pointAmount = (int) (d / 0.04f);

			if (pointAmount < 3)
			{
				pointAmount = 3;
			}
			List<Vector3> points = new();
			for (int i = 0; i < pointAmount + 1; i++)
			{
				float p = i / (float) pointAmount;
				float newAngle = Mathf.LerpAngle(phiFrom, phiTo, p);
				float r = -0.4f * Mathf.Pow(p - 0.5f, 2) + 0.1f + _planetRadius;
                Vector3 planePosition = new()
                {
                    X = r * Mathf.Cos(newAngle),
                    Z = r * Mathf.Sin(newAngle)
                };
                Vector3 position = planePosition;
				points.Add(position);
			}
			return points;
		}


		private void BuildRouteGeometry(List<Vector3> points)
		{
			Image image = Image.Create(points.Count, 1, false, Image.Format.Rgbf);
			int i = 0;
			float length = 0.0f;
			foreach (Vector3 point in points)
			{
				Color color = new(point.X, point.Y, point.Z);
				image.SetPixel(i, 0, color);
				i++;
			}
			GpuParticles3D particles = GetNode<GpuParticles3D>("Particles");
			GpuParticles3D routeParticles = GetNode<GpuParticles3D>("Line");
			ShaderMaterial particleMaterial = (ShaderMaterial) particles.ProcessMaterial.Duplicate(true);
			ImageTexture texture = ImageTexture.CreateFromImage(image);

			particles.Lifetime = Length / TransportSpeed;
			particleMaterial.SetShaderParameter("points", texture);
			particleMaterial.SetShaderParameter("speed", TransportSpeed);
			particleMaterial.SetShaderParameter("route_length", length);
			particles.ProcessMaterial = particleMaterial;
			particles.Restart();
			routeParticles.ProcessMaterial = particleMaterial;
			routeParticles.Restart();
		}

		private void BuildRouteCollisions(List<Vector3> points)
		{
            CapsuleShape3D shape = new()
            {
                Radius = 0.01f,
                Height = Length / points.Count
            };
            shape.Height *= 1.125f;
			for (int i = 0; i < points.Count; i++)
			{
                CollisionShape3D collisionShape = new();
                collisionShape.Shape = shape;
				Vector3 lookPoint;
				if (i == points.Count - 1)
				{
					lookPoint = points[i - 1];
				}
				else
				{
					lookPoint = points[i + 1];
				}
                AddChild(collisionShape);
				Transform3D transform = collisionShape.Transform;
				transform.Origin = points[i].Lerp(lookPoint, 0.5f);
				transform = transform.LookingAt(lookPoint, Vector3.Left);
				transform.Basis = transform.Basis.Rotated(Vector3.Up, (float) (Math.PI * 0.5));
				collisionShape.Transform = transform;
			}
		}

		public override void Destroy()
		{
			GD.Print("DESTROY");
			if (!Start.IsQueuedForDeletion())
			{
				Start.RemoveRouteConnection(this);
			}
			if (!Ending.IsQueuedForDeletion())
			{
				Ending.RemoveRouteConnection(this);
			}
			Ending.RemoveRouteConnection(this);
			base.Destroy();
		}
	}

}
