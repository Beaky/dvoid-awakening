namespace View.ViewStates
{
	using Godot;
	using Entities;
	using View.PlanetView;
	using View.PlanetView.Structures;
	using View.PlanetView.Ui;
	using System;
	using Godot.Collections;
	using System.Linq;
	using System.Collections.Generic;

	public partial class PlanetViewState : ViewState
	{
		enum ConstructionCategories
		{
			Resources,
			Industry,
			Civillian
		}

		[Export]
		private string _cityModelsScenePath = "";

		[Export]
		private string _routeModelPath = "";

		private Node _cityModelLibrary;
		private PlanetConstructionBuilding _constructionBuilding;
		private PackedScene _transportRouteResource;
		private PlanetViewUi _ui;
		public Planet Planet;
		private PlanetBuilding _placingCity;
		private PlanetTransportRoute _placingRoute;

		public override void _Ready()
		{
			_ui = GetNode<PlanetViewUi>("PlanetViewUi");
			_ui.Visible = false;
			_cityModelLibrary = ResourceLoader.Load<PackedScene>(_cityModelsScenePath).Instantiate();
			_transportRouteResource = ResourceLoader.Load<PackedScene>(_routeModelPath);
			_ui.BuildConstructionButtons(_cityModelLibrary);
			SetProcess(false);
			SetProcessInput(false);
		}

		public override void Enter(ViewState previousState)
		{
			_ui.Visible = true;
			SetProcess(true);
			SetProcessInput(true);
		}

		public void OnConstructionButtonPressed(PlanetBuilding building)
		{
			_constructionBuilding = null;
			if (_placingCity != null)
			{
				_placingCity.QueueFree();
				_placingCity = null;
			}
			_placingCity = (PlanetBuilding) building.Duplicate();
			Planet.AddChild(_placingCity);
			if (_placingCity.ConstructionRecipe != null) {
				_constructionBuilding = (PlanetConstructionBuilding) _cityModelLibrary.GetNode<PlanetConstructionBuilding>("Hidden/ConstructionSite").Duplicate();
				Planet.AddChild(_constructionBuilding);
			}
		}

		private void OnTransportButtonPressed()
		{
			_placingRoute = (PlanetTransportRoute) _transportRouteResource.Instantiate();
			Planet.AddChild(_placingRoute);
			GetNode<Label>("PlanetViewUi/MessageLabel").Text = "Select first node, then select second node.";
		}

		public override void _Process(double delta)
		{
			if (_placingCity != null)
			{
				_placingCity.GlobalTransform = GetCityPlanetTransform(_placingCity, GetTree().Root.GetMousePosition());
			}
		}

		public override void _Input(InputEvent @event)
		{
			switch (@event) {
				case InputEventKey key:
					if (key.IsActionPressed("ui_cancel"))
					{
						ToSystemViewState();
					}
					break;
				case InputEventMouseMotion mouseMotionEvent:
					// Panning over the planet.
					if (Input.IsMouseButtonPressed(MouseButton.Middle) && !_ui.PlanetBuildingUiInfoIsVisible())
					{
						Transform3D parentTransform = ViewCamera.GetParent<Node3D>().GlobalTransform;

						Vector3 parentBasisEuler = parentTransform.Basis.GetEuler();
						parentBasisEuler.X = Mathf.Clamp(parentBasisEuler.X -mouseMotionEvent.Relative.Y * _mouseSensitivity, _minimumPitch, _maximumPitch);
						parentBasisEuler.Y -= mouseMotionEvent.Relative.X * _mouseSensitivity;
						parentTransform.Basis = Basis.FromEuler(parentBasisEuler);

						ViewCamera.GetParent<Node3D>().GlobalTransform = parentTransform;
					}
					break;
				case InputEventMouseButton mouseButtonEvent:
					if (_ui.PlanetBuildingUiInfoIsVisible())
					{
						return;
					}
					// Zooming in and out
					if (mouseButtonEvent.ButtonIndex == MouseButton.WheelUp || mouseButtonEvent.ButtonIndex == MouseButton.WheelDown)
					{
						float fieldOfView = ViewCamera.Fov;
						fieldOfView += mouseButtonEvent.ButtonIndex == MouseButton.WheelUp ? -1 * _zoomSensitivity : 1 * _zoomSensitivity;
						ViewCamera.Fov = Mathf.Clamp(fieldOfView, 45.0f, 145.0f);
					}
					// City placement
					if (_placingCity != null && !mouseButtonEvent.Pressed)
					{
						if (mouseButtonEvent.ButtonIndex == MouseButton.Left && _placingCity.ValidPlacement)
						{
							if (_constructionBuilding != null)
							{
								_constructionBuilding.Transform = _placingCity.Transform;
								Planet.RegisterBuilding(_constructionBuilding);
								_constructionBuilding.Build(_placingCity);
							}
							else
							{
								Planet.RegisterBuilding(_placingCity);
							}
							_placingCity.GetNode<Area3D>("Area3D").QueueFree();
							_placingCity = null;
						}
						else if (mouseButtonEvent.ButtonIndex == MouseButton.Right && !mouseButtonEvent.Pressed)
						{
							_constructionBuilding?.QueueFree();
							_placingCity.QueueFree();
							_placingCity = null;
						}
					}
					// Transport route placement
					else if (_placingRoute != null)
					{
						if (mouseButtonEvent.ButtonIndex == MouseButton.Left && !mouseButtonEvent.Pressed)
						{
							if (PlaceTransportRoute(_placingRoute, mouseButtonEvent.Position))
							{
								if (_placingRoute.Ending != null)
								{
									Planet.RegisterTransportRoute(_placingRoute);
									_placingRoute.Build();
									_placingRoute = null;
									GetNode<Label>("PlanetViewUi/MessageLabel").Text = "";
								}

							}
						}
						else if (mouseButtonEvent.ButtonIndex == MouseButton.Right)
						{
							_placingRoute.QueueFree();
							_placingRoute = null;
							GetNode<Label>("PlanetViewUi/MessageLabel").Text = "";
						}
					}
					else if (mouseButtonEvent.ButtonIndex == MouseButton.Left && !mouseButtonEvent.Pressed)
					{
						PlanetStructure structure = GetPlanetStructureUnderCursor(mouseButtonEvent.Position);
						if(structure != null)
						{
							_ui.ShowPlanetStructureUi(structure);
						}
					}
					break;
			}
		}

		/// <summary>
		/// Attempt to place the transport route (Either set the start or end).
		/// </summary>
		/// <returns>
		/// false if placement was unsuccessfull.
		/// true if placement was successfull
		/// </returns>
		private bool PlaceTransportRoute(PlanetTransportRoute route, Vector2 mousePosition)
		{
			PhysicsDirectSpaceState3D space = GetWindow().World3D.DirectSpaceState;
			Vector3 to = ViewCamera.ProjectPosition(mousePosition, 50.0f);
			PhysicsRayQueryParameters3D query = PhysicsRayQueryParameters3D.Create(ViewCamera.GlobalTransform.Origin, to);
			Godot.Collections.Dictionary gdResult = space.IntersectRay(query);
			if (gdResult.Count == 0)
			{
				return false;
			}
			else if ((GodotObject) gdResult["collider"] is PlanetBuilding building)
			{
				return route.ConnectToBuilding(building);
			}
			else
			{
				return false;
			}
		}

		private Transform3D GetCityPlanetTransform(StaticBody3D city, Vector2 mousePosition)
		{
			PhysicsDirectSpaceState3D space = GetWindow().World3D.DirectSpaceState;
			Vector3 to = ViewCamera.ProjectPosition(mousePosition, 50.0f);
			Array<Node> nodes = GetTree().GetNodesInGroup("PlanetBuildings");
			Array<Rid> nodeRids = new();
			foreach (CollisionObject3D node in nodes.Cast<CollisionObject3D>())
			{
				nodeRids.Add(node.GetRid());
			}
			PhysicsRayQueryParameters3D query = PhysicsRayQueryParameters3D.Create(ViewCamera.GlobalTransform.Origin, to, exclude: nodeRids);
			Godot.Collections.Dictionary gdResult = space.IntersectRay(query);
			if (gdResult.Count == 0)
			{
				return city.GlobalTransform;
			}

			Transform3D cityTransform = city.GlobalTransform;
			cityTransform.Origin = (Vector3) gdResult["position"];
			cityTransform = cityTransform.LookingAt(cityTransform.Origin + (Vector3) gdResult["normal"], cityTransform.Basis.Y.Normalized());

			return cityTransform;
		}

		private PlanetStructure GetPlanetStructureUnderCursor(Vector2 mousePosition)
		{
			PhysicsDirectSpaceState3D space = GetWindow().World3D.DirectSpaceState;
			Vector3 to = ViewCamera.ProjectPosition(mousePosition, 50.0f);
			PhysicsRayQueryParameters3D query = PhysicsRayQueryParameters3D.Create(ViewCamera.GlobalTransform.Origin, to, 0b10);
			Godot.Collections.Dictionary gdResult = space.IntersectRay(query); // Check for buildings first
			if (gdResult.Count != 0 && (GodotObject) gdResult["collider"] is PlanetBuilding building)
			{
				return building;
			}
			else
			{
				query.CollisionMask = 0b100;
				gdResult = space.IntersectRay(query); // Check for transport routes next
				if (gdResult.Count != 0 && (GodotObject) gdResult["collider"] is PlanetTransportRoute transportRoute)
				{
					return transportRoute;
				}
				else
				{
					return null;
				}
			}
		}

		private void ToSystemViewState()
		{
			_ui.Visible = false;
			Node3D cameraPivot = ViewCamera.GetParent<Node3D>();
			Tween tween = GetTree().CreateTween();
			tween.Parallel().TweenProperty(cameraPivot, "global_position", new Vector3(0,0,0), ViewController.TransitionDuration);
			tween.Parallel().TweenProperty(ViewCamera, "position", new Vector3(0, 0, 150), ViewController.TransitionDuration);
			tween.TweenCallback(Callable.From(() => SystemTweenFinished()));
		}

		private void SystemTweenFinished()
		{
			ViewController.ActiveViewState = ViewController.GetNode<SystemViewState>("States/SystemViewState");
		}

		public override void Exit()
		{
			SetProcessInput(false);
			SetProcess(false);
		}
	}
}
