namespace View.ViewStates
{
    using Godot;
    using Entities;
    using System;
    using System.Linq;

    public partial class SystemViewState : ViewState
	{
		private Control _ui;
		[Export]
		private Control _planetLabelContainer;
		[Export]
		private Control _fleetLabelContainer;
		private Fleet _selectedFleet;
		[Export]
		private StaticBody3D _movementPlane;
		[Export]
		private Node3D _fleetLine;

		public override void _Ready()
		{
			_ui = GetNode<Control>("SystemViewUi");
			SetProcess(false);
			SetProcessInput(false);
		}

		public override void Enter(ViewState previousState)
		{
			SetProcessInput(true);
			SetProcess(true);
			UpdatePlanetLabels();
			_ui.Visible = true;
		}

		public override void _Process(double delta)
		{
			UpdatePlanetLabels();
			UpdateFleetLabels();
		}

		public override void _Input(InputEvent @event)
		{
			if (@event is InputEventMouseMotion mouseMotionEvent)
			{
				// Panning over the System.
				if (Input.IsMouseButtonPressed(MouseButton.Middle))
				{
					Transform3D parentTransform = ViewCamera.GetParent<Node3D>().GlobalTransform;

					Vector3 parentBasisEuler = parentTransform.Basis.GetEuler();
					parentBasisEuler.X = Mathf.Clamp(parentBasisEuler.X -mouseMotionEvent.Relative.Y * _mouseSensitivity, _minimumPitch, _maximumPitch);
					parentBasisEuler.Y -= mouseMotionEvent.Relative.X * _mouseSensitivity;
					parentTransform.Basis = Basis.FromEuler(parentBasisEuler);

					ViewCamera.GetParent<Node3D>().GlobalTransform = parentTransform;
				}
				else if (Input.IsMouseButtonPressed(MouseButton.Right) && _selectedFleet != null)
                {
                    ShowFleetMovementSelection(mouseMotionEvent.Position);
                }
            }
			else if (@event is InputEventMouseButton mouseButtonEvent)
			{
				// Zooming in and out
				if (mouseButtonEvent.ButtonIndex == MouseButton.WheelUp || mouseButtonEvent.ButtonIndex == MouseButton.WheelDown)
				{
					float fieldOfView = ViewCamera.Fov;
					fieldOfView += mouseButtonEvent.ButtonIndex == MouseButton.WheelUp ? -1 * _zoomSensitivity : 1 * _zoomSensitivity;
					ViewCamera.Fov = Mathf.Clamp(fieldOfView, 45.0f, 145.0f);
				}
				if (mouseButtonEvent.ButtonIndex == MouseButton.Right && mouseButtonEvent.IsPressed())
				{
					if (_selectedFleet != null)
					{
						PhysicsDirectSpaceState3D space = GetWindow().World3D.DirectSpaceState;
						Vector3 to = ViewCamera.ProjectPosition(mouseButtonEvent.GlobalPosition, 1000.0f);
						PhysicsRayQueryParameters3D query = PhysicsRayQueryParameters3D.Create(ViewCamera.GlobalTransform.Origin, to, collisionMask: 2);
						Godot.Collections.Dictionary gdResult = space.IntersectRay(query);
						_movementPlane.GlobalPosition = (Vector3) gdResult["position"] * new Vector3(1, 0, 1);
						Node3D line = _movementPlane.GetNode<Node3D>("Line");
						line.Call("clear_points");
						line.Visible = false;
						_movementPlane.Visible = true;
					}
				}
				if (mouseButtonEvent.ButtonIndex == MouseButton.Right && mouseButtonEvent.IsReleased())
				{
					GD.Print(_selectedFleet);
					if (_selectedFleet != null) 
					{
						_selectedFleet.TargetPosition = GetMovementTarget(mouseButtonEvent.Position);
						_movementPlane.Hide();
						_fleetLine.Call("set_points", _selectedFleet.MovementCurve.GetBakedPoints());
					}
				}
			}
			else if (@event is InputEventKey keyEvent)
			{
				if (keyEvent.IsActionReleased("ui_cancel") && _selectedFleet != null)
				{
					DeselectFleet();
				}
			}
		}

        private void ShowFleetMovementSelection(Vector2 mousePosition)
        {
            Vector3 point = GetMovementTarget(mousePosition) * Vector3.Up;

            Node3D line = _movementPlane.GetNode<Node3D>("Line");
            Vector3 lineEnd = Vector3.Zero;
            if ((int)line.Call("get_point_count") > 0)
            {
                lineEnd = (Vector3)line.Call("get_point", 1);
            }
            if (Math.Sign(lineEnd.Y) != Math.Sign(point.Y))
            {
                Shader line3DShader = (Shader)GD.Load<Shader>("res://addons/godot-polyliner/shaders/line_glow.tres").Duplicate();
                ShaderMaterial shaderMaterial = new();
                if (point.Y > 0)
                {
                    shaderMaterial.SetShaderParameter("color", new Color(0.0f, 1.0f, 0.25f));
                }
                else
                {
                    shaderMaterial.SetShaderParameter("color", new Color(1.0f, 0.25f, 0.0f));
                }
                shaderMaterial.Shader = line3DShader;
                line.Call("set_material", shaderMaterial);
            }
            line.Call("set_points", new Vector3[] { Vector3.Zero, point });
			line.Visible = true;
        }

        private Vector3 GetMovementTarget(Vector2 mousePosition)
        {
            Vector3 planeNormal = ViewCamera.GlobalPosition - _movementPlane.GlobalPosition;
            planeNormal.Y = 0;
            planeNormal = planeNormal.Normalized();
            Plane plane = new(planeNormal, _movementPlane.GlobalPosition);

            Vector3 mousePoint = ViewCamera.ProjectPosition(mousePosition, 1.0f);
            Vector3 mouseDirection = (mousePoint - ViewCamera.GlobalPosition).Normalized();
            Vector3 point = (Vector3)plane.IntersectsRay(ViewCamera.GlobalPosition, mouseDirection);
            return point;
        }

        private void UpdatePlanetLabels()
		{
			Boolean firstRun = _planetLabelContainer.GetChildCount() == 0;
			foreach(Planet planet in GetTree().GetNodesInGroup("Planets").Cast<Planet>())
			{
				Button label = _planetLabelContainer.GetNodeOrNull<Button>((String) planet.Name);;
				if (firstRun)
				{
                    label = new()
                    {
                        Name = planet.Name,
                        Text = planet.Name
                    };
                    _planetLabelContainer.AddChild(label);
					label.Pressed += () => PlanetLabelPressed(planet);
                }
                label.Position = ViewCamera.UnprojectPosition(planet.GlobalPosition) + new Vector2(20, 0);
			}
		}

		private void UpdateFleetLabels()
		{
			StyleBox pressedStyle = GD.Load<StyleBox>("res://UiThemes/Styles/ToggleButtonPressed.tres");
			foreach(Fleet fleet in GetTree().GetNodesInGroup("Fleets").Cast<Fleet>())
			{
				Button label = _fleetLabelContainer.GetNodeOrNull<Button>((String) fleet.Name);
				if (label == null)
				{
                    label = new()
                    {
                        Name = fleet.Name,
                        Text = fleet.Name,
						ToggleMode = true
                    };
					label.AddThemeStyleboxOverride("pressed", pressedStyle);
                    _fleetLabelContainer.AddChild(label);
					label.Pressed += () => FleetLabelPressed(label, fleet);
                }
                label.Position = ViewCamera.UnprojectPosition(fleet.GlobalPosition) + new Vector2(20, 0);
			}
		}

		private void PlanetLabelPressed(Planet planet)
		{
			_ui.Visible = false;
			Node3D cameraPivot = ViewCamera.GetParent<Node3D>();
			Tween tween = GetTree().CreateTween();
			tween.Parallel().TweenProperty(cameraPivot, "global_position", planet.GlobalPosition, ViewController.TransitionDuration);
			tween.Parallel().TweenProperty(ViewCamera, "position", new Vector3(0, 0, 6), ViewController.TransitionDuration);
			tween.TweenCallback(Callable.From(() => PlanetTweenFinished(planet)));
		}

		private void FleetLabelPressed(Button button, Fleet fleet)
		{
			if (button.ButtonPressed)
			{
				SelectFleet(fleet);
			}
			else
			{
				DeselectFleet();
			}
		}

		private void SelectFleet(Fleet fleet)
		{
			_movementPlane.GlobalPosition = fleet.GlobalPosition;
			_selectedFleet = fleet;

			if (_selectedFleet.MovementCurve.PointCount > 0)
			{
				_fleetLine.Call("set_points", _selectedFleet.MovementCurve.GetBakedPoints());
				_fleetLine.Visible = true;
			}
		}

		private void DeselectFleet()
		{
			Button button = _fleetLabelContainer.GetNode<Button>((string) _selectedFleet.Name);
			button.ButtonPressed = false;
			_movementPlane.Visible = false;
			_fleetLine.Visible = false;
			_selectedFleet = null;
		}

		private void PlanetTweenFinished(Planet planet)
		{
			PlanetViewState newState = ViewController.GetNode<PlanetViewState>("States/PlanetViewState");
			newState.Planet = planet;
            ViewController.ActiveViewState = newState;
		}

		public override void Exit()
		{
			SetProcessInput(false);
			SetProcess(false);
		}
	}
}
