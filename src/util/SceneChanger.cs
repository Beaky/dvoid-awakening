using Godot;
using Godot.Collections;
using static Godot.ResourceLoader;

public partial class SceneChanger : Node
{
	[Export]
	private string _defaultScene = "";
	private string _queuedScene;

	private Node _scenesRoot;
	private Control _loadingScreen;
	private ProgressBar _loadingProgress;
	private Label _errorLabel;
	private AnimationPlayer _fadePlayer;


	public override void _Ready()
	{
		_scenesRoot = (Node)GetNode("Scenes");
		_loadingScreen = (Control)GetNode("LoadingScreen");
		_loadingProgress = (ProgressBar)GetNode("LoadingScreen/ProgressBar");
		_errorLabel = (Label)GetNode("LoadingScreen/Label");
		_fadePlayer = (AnimationPlayer)GetNode("LoadingScreen/AnimationPlayer");


		SetProcess(false);
		_loadingScreen.Visible = false;
		if (_defaultScene.Length != 0)
		{
			ChangeSceneToFile(_defaultScene);
		}
	}


	public override void _Process(double delta)
	{
		Array loadProgress = new();
		ThreadLoadStatus status = ResourceLoader.LoadThreadedGetStatus(_queuedScene, loadProgress);
		if (status == ThreadLoadStatus.Failed)
		{
			GD.PrintErr("Failed to load scene!");
		}
		if (status == ThreadLoadStatus.Loaded)
		{
			SetProcess(false);
			ChangeSceneToPacked((PackedScene) ResourceLoader.LoadThreadedGet(_queuedScene));
		}
	}


	public void ChangeSceneToFile(string scenePath)
	{
		_queuedScene = scenePath;
		_fadePlayer.Play("fadein");
		_loadingProgress.Value = 0.0;
	}


	private void ChangeSceneToPacked(PackedScene scene)
	{
		_scenesRoot.AddChild(scene.Instantiate());
		_fadePlayer.Play("fadeout");
	}


	private void LoadScene(string scenePath)
	{
		ResourceLoader.LoadThreadedRequest(scenePath, "PackedScene");
		_loadingProgress.Value = 0;
		_loadingProgress.MaxValue = 100;
		SetProcess(true);
	}


	private void OnAnimationPlayerAnimationFinished(string animationName)
	{
		if (animationName == "fadein")
		{
			LoadScene(_queuedScene);
		}
	}
}
